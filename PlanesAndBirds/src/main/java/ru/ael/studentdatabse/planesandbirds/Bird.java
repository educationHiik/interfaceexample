/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package ru.ael.studentdatabse.planesandbirds;

import java.util.logging.Logger;

/**
 *
 * @author developer
 */

//            имя класса       имя интерфейса
//             |               /
public class Bird implements Fly {

    private static final Logger log = Logger.getLogger(Bird.class.getName());

    
    @Override
    public void takeOff() {
        log.info("Начать разбег");
        
    }

    @Override
    public void fly() {
        log.info("Взмахи крыльев");        
    }

    @Override
    public void landing() {
        log.info("Прератить взмахи крыльев, певести ноги в вертикальное положение и начать планирование");
    }
    
}
