/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package ru.ael.studentdatabse.planesandbirds;

/**
 *
 * @author developer
 */
public class Dispatcher {
    
    public void permitTakeoff(Fly object)
   {
       object.takeOff();   
   }        
  
    
    
   public void permitFly(Fly object)
   {
       object.fly();   
   }        
 
   
   
    
}
