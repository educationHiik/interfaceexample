/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package ru.ael.studentdatabse.planesandbirds;

/**
 * Интерсейс описывет действия путем указания 
 * сигнатуры метода 
 * 
 * Сигнатура методо определяется входными данными и выходными данными метода
 * 
 * @author developer
 */
public interface Fly {
    
    public void takeOff();  // Взлет
    public void fly();      // Полет
    public void landing();  // Посадка
    
}
