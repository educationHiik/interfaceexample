/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package ru.ael.studentdatabse.planesandbirds;

import java.util.logging.Logger;

/**
 *
 * @author developer
 */
public class Plane implements Fly {

    private static final Logger log = Logger.getLogger(Plane.class.getName());

    @Override
    public void takeOff() {
        log.info("Включить двигатели и начать разбег");        
    }

    @Override
    public void fly() {
        log.info("Двигатели работают");        
    }

    @Override
    public void landing() {
       log.info("Выпустить закрылки, выпукают шасси, снижают обороты двигатели");        
    }
    
    
    
}
