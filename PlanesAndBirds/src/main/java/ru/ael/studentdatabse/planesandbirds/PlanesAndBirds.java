/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Project/Maven2/JavaApp/src/main/java/${packagePath}/${mainClassName}.java to edit this template
 */
package ru.ael.studentdatabse.planesandbirds;

import java.util.logging.Logger;

/**
 *
 * @author developer
 */
public class PlanesAndBirds {

    private static final Logger log = Logger.getLogger(PlanesAndBirds.class.getName());

    public static void main(String[] args) {

        log.info("Программа моделирования полета птиц и самолетов");

        Bird bird = new Bird();
        Plane plane = new Plane();
        Cwadrocopter cwadrocopter = new Cwadrocopter();
        
        
        Dispatcher dispatcher = new Dispatcher();
        dispatcher.permitTakeoff(bird);
        dispatcher.permitTakeoff(plane);
        dispatcher.permitTakeoff(cwadrocopter);
        

    }
}
